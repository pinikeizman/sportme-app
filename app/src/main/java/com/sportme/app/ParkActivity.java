package com.sportme.app;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sportme.app.async.GetParkHttpRequest;
import com.sportme.app.async.OpenDeviceAsnTask;
import com.sportme.app.async.SetParkRatingForUserHttpTask;
import com.sportme.app.model.Park;
import com.sportme.app.model.UserParkRating;
import com.sportme.app.utils.UtilsHelper;


/**
 * Created by pinhask on 7/25/17.
 */

public class ParkActivity extends AppCompatActivity {

    AppData DB = AppData.getInstance();
    AlertDialog dialog;
    private Gson gson = new Gson();

    View.OnClickListener ocl = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            try {
                String parkid = DB.getString(UtilsHelper.CURRENT_PARK_ID);
                Park current = UtilsHelper.parks.get(parkid);
                View theView = LayoutInflater.from(ParkActivity.this).inflate(R.layout.park_rating_pupup_selection, null);

                TextView header = (TextView) theView.findViewById(R.id.lbl_rating_popup_header);
                Spinner spn = (Spinner) theView.findViewById(R.id.spnRating);
                ImageView rating = (ImageView) theView.findViewById(R.id.img_rating_popup);

                // Create an ArrayAdapter using the string array and a default spinner layout
                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getBaseContext(),
                        R.array.planets_array, android.R.layout.simple_spinner_item);
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                // Apply the adapter to the spinner
                spn.setAdapter(adapter);
                OpenDeviceAsnTask open = new OpenDeviceAsnTask(dialog,ParkActivity.this, rating, theView);

                // Set initials Values
                spn.setOnTouchListener(UtilsHelper.getRatingOnTouchListiner());
                spn.setOnItemSelectedListener(open);

                // If user didn't rate yet.
                if (current.getUserRating() != null) {
                    rating.setImageResource(UtilsHelper.getRatingBigImage(current.getUserRating()));
                    spn.setSelection(current.getUserRating() - 1, false);
                } else {
                    header.setVisibility(View.INVISIBLE);
                    spn.setSelection(1, false);

                }
                AlertDialog.Builder builder = new AlertDialog.Builder(ParkActivity.this);

                builder.setView(theView);
                dialog = builder.create();
                open.setDialog(dialog);
                dialog.show();

            } catch (Exception r) {
                r.printStackTrace();
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImageView rating;

        setContentView(R.layout.park_layout);

        String parkid = DB.getString(UtilsHelper.CURRENT_PARK_ID);
        String userid = DB.getString(UtilsHelper.CURRENT_USER_ID);
        //async data load
        new GetParkHttpRequest(getBaseContext(), this).execute(userid, parkid);
        rating = (ImageView) findViewById(R.id.imgParkRating);
        rating.setOnClickListener(ocl);

    }


}
