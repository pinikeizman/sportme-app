package com.sportme.app.search;

/**
 * Created by pinhask on 7/18/17.
 */

public interface Viewable {

    String getIcon();
    String getDescription();
    String getHeader();
    String getRating();


}
