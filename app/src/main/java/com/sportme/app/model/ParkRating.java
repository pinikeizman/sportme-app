package com.sportme.app.model;

import java.io.Serializable;

/**
 * Created by pinhask on 7/28/17.
 */

public class ParkRating implements Serializable {

    private String parkId;
    private Integer numOfRating;
    private String rating;

    public ParkRating() {
    }

    public ParkRating(String parkId, Integer numOfRating, String rating) {
        this.parkId = parkId;
        this.numOfRating = numOfRating;
        this.rating = rating;
    }

    public String getParkId() {
        return parkId;
    }

    public void setParkId(String parkId) {
        this.parkId = parkId;
    }

    public Integer getNumOfRating() {
        return numOfRating;
    }

    public void setNumOfRating(Integer numOfRating) {
        this.numOfRating = numOfRating;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
