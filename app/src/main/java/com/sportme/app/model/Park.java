package com.sportme.app.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pinhask on 7/18/17.
 */

public class Park implements Serializable {

    private int parkid;
    private String parkName;
    private String latitude;
    private String longitude;
    private String parkPicture;
    private String city;
    private String supportEmail;
    private String street;
    private String aboutPark;
    private Boolean isFollower;
    private Integer userRating;
    private List<List<Integer>> devicesId;
    private Integer numOfFollowers;
    private Integer numOfActivities;
    private Integer numOfRating;
    private String parkRating;

    public Park(int parkid, String parkName, String latitude, String longitude, String parkPicture, String city, String supportEmail, String street, String aboutPark, Boolean isFollower, Integer userRating, List<List<Integer>> devicesId, Integer numOfFollowers, Integer numOfActivities, Integer numOfRating, String parkRating) {
        this.parkid = parkid;
        this.parkName = parkName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.parkPicture = parkPicture;
        this.city = city;
        this.supportEmail = supportEmail;
        this.street = street;
        this.aboutPark = aboutPark;
        this.isFollower = isFollower;
        this.userRating = userRating;
        this.devicesId = devicesId;
        this.numOfFollowers = numOfFollowers;
        this.numOfActivities = numOfActivities;
        this.numOfRating = numOfRating;
        this.parkRating = parkRating;
    }

    public Park() {
    }

    public int getParkid() {
        return parkid;
    }

    public void setParkid(int parkid) {
        this.parkid = parkid;
    }

    public String getParkName() {
        return parkName;
    }

    public void setParkName(String parkName) {
        this.parkName = parkName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getParkPicture() {
        return parkPicture;
    }

    public void setParkPicture(String parkPicture) {
        this.parkPicture = parkPicture;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSupportEmail() {
        return supportEmail;
    }

    public void setSupportEmail(String supportEmail) {
        this.supportEmail = supportEmail;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAboutPark() {
        return aboutPark;
    }

    public void setAboutPark(String aboutPark) {
        this.aboutPark = aboutPark;
    }

    public Boolean getIsFollower() {
        return isFollower;
    }

    public void setIsFollower(Boolean isFollower) {
        this.isFollower = isFollower;
    }

    public Integer getUserRating() {
        return userRating;
    }

    public void setUserRating(Integer userRating) {
        this.userRating = userRating;
    }

    public List<List<Integer>> getDevicesId() {
        return devicesId;
    }

    public void setDevicesId(List<List<Integer>> devicesId) {
        this.devicesId = devicesId;
    }

    public Integer getNumOfFollowers() {
        return numOfFollowers == null ? 0 : numOfFollowers;
    }

    public void setNumOfFollowers(Integer numOfFollowers) {
        this.numOfFollowers = numOfFollowers;
    }

    public Integer getNumOfActivities() {
        return numOfActivities == null ? 0 : numOfActivities;

    }

    public void setNumOfActivities(Integer numOfActivities) {
        this.numOfActivities = numOfActivities;
    }

    public Integer getNumOfRating() {
        return numOfRating == null ? 0 : numOfRating;
    }

    public void setNumOfRating(Integer numOfRating) {
        this.numOfRating = numOfRating;
    }

    public String getParkRating() {
        return parkRating;
    }

    public void setParkRating(String parkRating) {
        this.parkRating = parkRating;
    }
}