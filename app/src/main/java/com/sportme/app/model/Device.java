package com.sportme.app.model;

import android.bluetooth.BluetoothClass;

import com.sportme.app.Interfaces.SearchEntitiy;

import java.io.Serializable;

/**
 * Created by pinhask on 7/29/17.
 */

public class Device implements SearchEntitiy, Serializable {

    private String deviceId;
    private String deviceName;
    private String catalogNumber;
    private String DeviceType;
    private String RecommendedProgram;
    private String DeviceManufacturer;
    private String DevicePicture;
    private String deviceNumber;
    private String rating;
    private String numOfRating;
    private String status;
    private String userRating;

    public Device() {
    }

    public Device(String deviceId, String deviceName, String catalogNumber, String deviceType, String recommendedProgram, String deviceManufacturer, String devicePicture, String deviceNumber, String rating, String numOfRating, String status, String userRating) {
        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.catalogNumber = catalogNumber;
        DeviceType = deviceType;
        RecommendedProgram = recommendedProgram;
        DeviceManufacturer = deviceManufacturer;
        DevicePicture = devicePicture;
        this.deviceNumber = deviceNumber;
        this.rating = rating;
        this.numOfRating = numOfRating;
        this.status = status;
        this.userRating = userRating;
    }

    public String getUserRating() {
        return userRating;
    }

    public void setUserRating(String userRating) {
        this.userRating = userRating;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    @Override
    public String getPicture() {
        return this.DevicePicture;
    }

    @Override
    public String getHeader() {
        return getDeviceName();
    }

    @Override
    public String getDescription() {
        return getDeviceType();
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getNumOfRating() {
        return numOfRating;
    }

    public void setNumOfRating(String numOfRating) {
        this.numOfRating = numOfRating;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getCatalogNumber() {
        return catalogNumber;
    }

    public void setCatalogNumber(String catalogNumber) {
        this.catalogNumber = catalogNumber;
    }

    public String getDeviceType() {
        return DeviceType;
    }

    public void setDeviceType(String deviceType) {
        DeviceType = deviceType;
    }

    public String getRecommendedProgram() {
        return RecommendedProgram;
    }

    public void setRecommendedProgram(String recommendedProgram) {
        RecommendedProgram = recommendedProgram;
    }

    public String getDeviceManufacturer() {
        return DeviceManufacturer;
    }

    public void setDeviceManufacturer(String deviceManufacturer) {
        DeviceManufacturer = deviceManufacturer;
    }

    public String getDevicePicture() {
        return DevicePicture;
    }

    public void setDevicePicture(String devicePicture) {
        DevicePicture = devicePicture;
    }
}
