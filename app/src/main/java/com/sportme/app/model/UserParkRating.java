package com.sportme.app.model;

import java.io.Serializable;

/**
 * Created by pinhask on 7/28/17.
 */

public class UserParkRating implements Serializable {

    private RatingPK ratingId;
    private String rating;

    public UserParkRating() {
    }
    public UserParkRating(RatingPK rId, String rating) {
        this.ratingId = rId;
        this.rating = rating;
    }
    public UserParkRating(int parkId, int userId, String rating) {
        ratingId = new RatingPK(parkId, userId);
        this.rating = rating;
    }

    public RatingPK getRatingId() {
        return ratingId;
    }

    public void setRatingId(RatingPK ratingId) {
        this.ratingId = ratingId;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}


