package com.sportme.app.model;

import com.sportme.app.Interfaces.SearchEntitiy;

import java.io.Serializable;

/**
 * Created by pinhask on 7/29/17.
 */

public class DeviceEntity implements Serializable,SearchEntitiy {

    private String deviceNumber;
    private String deviceId;
    private String status;
    private String parkId;

    public DeviceEntity(){}
    public DeviceEntity(String deviceNumber, String deviceId, String status, String parkId) {
        this.deviceNumber = deviceNumber;
        this.deviceId = deviceId;
        this.status = status;
        this.parkId = parkId;
    }

    @Override
    public String getPicture() {
        return getPicture();
    }

    @Override
    public String getHeader() {
        return getStatus();
    }

    @Override
    public String getDescription() {
        return getDeviceId();
    }

    @Override
    public String getRating() {
        return null;
    }

    @Override
    public String getNumOfRating() {
        return null;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getParkId() {
        return parkId;
    }

    public void setParkId(String parkId) {
        this.parkId = parkId;
    }
}
