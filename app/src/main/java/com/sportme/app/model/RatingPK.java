package com.sportme.app.model;

import java.io.Serializable;

/**
 * Created by pinhask on 7/28/17.
 */


public class RatingPK implements Serializable {

    private int parkId;
    private int userId;

    public RatingPK(){}

    public RatingPK(int parkid, int userid) {
        this.parkId = parkid;
        this.userId = userid;
    }

    public int getParkId() {
        return parkId;
    }

    public void setParkId(int parkId) {
        this.parkId = parkId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
