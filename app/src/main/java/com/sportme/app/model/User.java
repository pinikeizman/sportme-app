package com.sportme.app.model;

import java.io.Serializable;

/**
 * Created by pinhask on 7/27/17.
 */

public class User implements Serializable {

    private Integer userId;
    private String userName;
    private String userCity;
    private String userPicture;
    private String userGender;
    private String firstName;
    private String lastName;
    private String address;
    private String userEmail;

    public User(Integer userId, String userName, String userCity, String userPicture, String userGender, String firstName, String lastName, String address, String userEmail) {
        this.userId = userId;
        this.userName = userName;
        this.userCity = userCity;
        this.userPicture = userPicture;
        this.userGender = userGender;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.userEmail = userEmail;
    }

    public User() {
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public String getUserPicture() {
        return userPicture;
    }

    public void setUserPicture(String userPicture) {
        this.userPicture = userPicture;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
