package com.sportme.app;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.sportme.app.adapters.PrkDevicesListAdapter;
import com.sportme.app.model.Device;
import com.sportme.app.model.Park;
import com.sportme.app.utils.DBApi;
import com.sportme.app.utils.UtilsHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pinhask on 7/29/17.
 */

public class DeviceFragment extends  Fragment{

    private AppData DB = AppData.getInstance();
    private DBApi api = new DBApi();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.park_devices_layout, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            Park currentPark = UtilsHelper.parks.get(DB.getString(UtilsHelper.CURRENT_PARK_ID));
            ListView deviceList = (ListView) getActivity().findViewById(R.id.devices_list_view);
            List<Device> devices = getParkDeviceList(currentPark);
            PrkDevicesListAdapter adapter = new PrkDevicesListAdapter(getActivity(), devices);
            deviceList.setAdapter(adapter);
        }catch (Exception e){
            Log.e(this.toString(),e.getMessage());
        }
    }
    private List<Device> getParkDeviceList(Park park) {
        ArrayList<Device> res = new ArrayList<>();
        for (int i = 0; i<park.getDevicesId().get(0).size();i++)
        {
            Integer temp = park.getDevicesId().get(0).get(i);
            Device device =UtilsHelper.devicesEntity.get(String.valueOf(temp));
            res.add(device);
        }
        return res;
    }
}
