package com.sportme.app;

import java.util.HashMap;
import java.util.Objects;

/**
 * Created by Doron Nacash on 06/28/17.
 */

public class AppData {

    private static final AppData ourInstance = new AppData();
    private final HashMap<String, Object> dataObject;

    private AppData() {
        dataObject = new HashMap<>();
    }

    public static AppData getInstance() {
        return ourInstance;
    }

    public Object addData(String key, Object value) {
        dataObject.put(key, value);
        return value;
    }

    public String getString(String key) {
        Object data = dataObject.get(key);
        return String.valueOf(data);
    }

    public Integer getInt(String key) {

        try {
            return Integer.valueOf(getString(key));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Integer getInt(Integer key) {

        try {
            return (Integer)dataObject.get(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void clearData() {
        dataObject.clear();
    }

    public Object getData(String key) {
        return dataObject.get(key);
    }
}
