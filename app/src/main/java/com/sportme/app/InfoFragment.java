package com.sportme.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.sportme.app.adapters.PrkDevicesListAdapter;
import com.sportme.app.model.Device;
import com.sportme.app.model.Park;
import com.sportme.app.utils.DBApi;
import com.sportme.app.utils.UtilsHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pinhask on 7/29/17.
 */

public class InfoFragment extends  Fragment{

    private AppData DB = AppData.getInstance();
    private DBApi api = new DBApi();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.park_info, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {

            DB.addData(UtilsHelper.CURRENT_PARK_TAB,R.id.btnActivitiesInfo);

            Park currentPark = UtilsHelper.parks.get(DB.getString(UtilsHelper.CURRENT_PARK_ID));

            TextView parkNumber = (TextView)getActivity().findViewById(R.id.lbl_park_info_park_number);
            TextView parkAddress = (TextView)getActivity().findViewById(R.id.lbl_park_info_park_address);
            TextView parkSupportInfo = (TextView)getActivity().findViewById(R.id.lbl_park_info_support_email);
            TextView parkAbout = (TextView)getActivity().findViewById(R.id.lbl_park_info_about);

            parkNumber.setText(String.valueOf(currentPark.getParkid()));
            parkAddress.setText(currentPark.getStreet());
            parkSupportInfo.setText(currentPark.getSupportEmail());
            parkAbout.setText(currentPark.getAboutPark());


        }catch (Exception e){
            Log.e(this.toString(),e.getMessage());
        }
    }

}
