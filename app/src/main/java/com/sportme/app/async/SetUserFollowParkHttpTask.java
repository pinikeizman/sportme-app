package com.sportme.app.async;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sportme.app.AppData;
import com.sportme.app.model.Device;
import com.sportme.app.model.Park;
import com.sportme.app.model.User;
import com.sportme.app.utils.DBApi;
import com.sportme.app.utils.UtilsHelper;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

/**
 * Created by pinhask on 1/27/17.
 */

public class SetUserFollowParkHttpTask extends AsyncTask<String, Void, Void> {

    private Context context;
    private ImageView followImg;
    private TextView numOfFollowers;
    private DBApi api = new DBApi();
    private Activity deviceActivity;

    public SetUserFollowParkHttpTask(ImageView followImg, TextView numOfFollowers, Activity deviceActivity) {
        this.followImg = followImg;
        this.numOfFollowers = numOfFollowers;
        this.deviceActivity = deviceActivity;
    }


    public Activity getDeviceActivity() {
        return (Activity) context;
    }

    @Override
    protected Void doInBackground(String... params) {

        String parkid = params[0];
        String userid = params[1];

        try {

            final ResponseEntity<List> response = api.setUserFollowPark(parkid, userid);

            if (response.getStatusCode() == HttpStatus.OK) {
                Runnable chageRating = new Runnable() {
                    @Override
                    public void run() {
                        ImageView imgFollow = followImg;
                        TextView numOfFollow = numOfFollowers;
                        followImg.setVisibility(View.INVISIBLE);
                        numOfFollow.setText(String.valueOf(response.getBody()));
                    }
                };
                deviceActivity.runOnUiThread(chageRating);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    protected void onPostExecute(Void v) {

    }

}
