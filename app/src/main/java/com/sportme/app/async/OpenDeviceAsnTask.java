package com.sportme.app.async;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sportme.app.AppData;
import com.sportme.app.ParkActivity;
import com.sportme.app.model.Park;
import com.sportme.app.utils.UtilsHelper;

/**
 * Created by pinhask on 1/19/17.
 */

public class OpenDeviceAsnTask extends AsyncTask<String, Void, Void> implements AdapterView.OnItemSelectedListener {
    private ImageView btnFollow = null;
    private AlertDialog dialog = null;
    private Activity activity = null;
    private View theView = null;
    private Gson gson = new Gson();
    private AppData DB = AppData.getInstance();

    public OpenDeviceAsnTask(AlertDialog dialog, Activity activity, ImageView image, View view) {
        this.theView = view;
        this.dialog = dialog;
        this.activity = activity;
        this.btnFollow = image;
    }

    protected Void doInBackground(String... urls) {
        try {

            //  Create dialog to be populated with the theView.


        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setDialog(AlertDialog dialog) {
        this.dialog = dialog;
    }

    protected void onPostExecute(Void i) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {

        if (DB.getData(UtilsHelper.RATING_SPINNER_TOUCH) == null
                || !(boolean) DB.getData(UtilsHelper.RATING_SPINNER_TOUCH)) {
            return;
        }

        TextView selection = (TextView) parent.getChildAt(0);
        String choise = selection.getText().toString();
        Park parkToUpdate = UtilsHelper.parks.get(DB.getString(UtilsHelper.CURRENT_PARK_ID));
        parkToUpdate.setUserRating(Integer.valueOf(choise));
        new SetParkRatingForUserHttpTask(dialog, activity, (ParkActivity) activity).execute(gson.toJson(parkToUpdate));
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
