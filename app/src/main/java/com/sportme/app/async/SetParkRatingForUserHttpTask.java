package com.sportme.app.async;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sportme.app.AppData;
import com.sportme.app.ParkActivity;
import com.sportme.app.R;
import com.sportme.app.model.Park;
import com.sportme.app.model.ParkRating;
import com.sportme.app.model.UserParkRating;
import com.sportme.app.model.User;
import com.sportme.app.utils.DBApi;
import com.sportme.app.utils.UtilsHelper;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;

/**
 * Created by pinhask on 1/27/17.
 */

public class SetParkRatingForUserHttpTask extends AsyncTask<String, Void, Void> {

    private Context context;
    private Activity parkActivity;
    private Spinner spn;
    private AppData DB = AppData.getInstance();
    private Map<String, Park> parks = UtilsHelper.parks;
    private Map<String, Bitmap> images = UtilsHelper.images;
    private Gson gson = new Gson();
    private AlertDialog dialog;
    private DBApi api = new DBApi();

    public SetParkRatingForUserHttpTask(AlertDialog dialog, Spinner spn, Context context, Activity parkActivity) {
        this.context = context;
        this.parkActivity = parkActivity;
        this.spn = spn;
        this.dialog = dialog;
    }

    public SetParkRatingForUserHttpTask(AlertDialog dialog, Context baseContext, ParkActivity parkActivity) {
        this.dialog = dialog;
        this.context = baseContext;
        this.parkActivity = parkActivity;
    }

    public Activity getParkActivity() {
        return (Activity) context;
    }

    @Override
    protected Void doInBackground(String... params) {


        try {

            final Park park = gson.fromJson(params[0], Park.class);
            final ParkRating parkRating = api.setUserRatingForPark(park);
            Runnable chageRating;
            // Merge data with cache and update view
            park.setParkRating(parkRating.getRating());
            park.setNumOfFollowers(parkRating.getNumOfRating());

            chageRating = new Runnable() {
                @Override
                public void run() {
                    ImageView imgRating = (ImageView) parkActivity.findViewById(R.id.imgParkRating);
                    TextView numOfRating = (TextView) parkActivity.findViewById(R.id.lbl_park_num_of_rating);
                    Integer val = park.getNumOfRating();
                    numOfRating.setText(val.toString());
                    imgRating.setImageResource(UtilsHelper.getRatingBigImage(park.getParkRating()));
                }
            };

            UtilsHelper.parks.put(String.valueOf(park.getParkid()), park);

            parkActivity.runOnUiThread(chageRating);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dialog.dismiss();
            DB.addData(UtilsHelper.RATING_SPINNER_TOUCH, false);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void v) {

    }

}
