package com.sportme.app.async;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sportme.app.AppData;
import com.sportme.app.DeviceFragment;
import com.sportme.app.InfoFragment;
import com.sportme.app.ParkActivity;
import com.sportme.app.R;
import com.sportme.app.model.Park;
import com.sportme.app.model.RatingPK;
import com.sportme.app.model.User;
import com.sportme.app.utils.DBApi;
import com.sportme.app.utils.UtilsHelper;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

/**
 * Created by pinhask on 1/27/17.
 */
public class GetParkHttpRequest extends AsyncTask<String, Void, Park> {

    Context context;
    Activity parkActivity;
    FragmentManager fragmentManager = null;
    AppData DB = AppData.getInstance();
    Map<String, Park> parks = UtilsHelper.parks;
    Map<String, Bitmap> images = UtilsHelper.images;

    Runnable hideLoading = new Runnable() {
        public void run() {
            // things need to work on ui thread
            View progressBar = parkActivity.findViewById(R.id.viewLoading);
            progressBar.setVisibility(View.INVISIBLE);
            return;
        }
    };
    Runnable showLoading = new Runnable() {
        public void run() {
            // things need to work on ui thread
            View progressBar = parkActivity.findViewById(R.id.viewLoading);
            progressBar.setVisibility(View.VISIBLE);
            return;
        }
    };

    public GetParkHttpRequest(Context context, Activity parkActivity) {
        this.context = context;
        this.parkActivity = parkActivity;
    }

    public Activity getParkActivity() {
        return (Activity) context;
    }

    @Override
    protected Park doInBackground(String... params) {

        parkActivity.runOnUiThread(showLoading);
        //String parkId = AppData.getInstance().getString(UtilsHelper.CURRENT_PARK);//params[0];
        Park res = null;
        DBApi api = new DBApi();
        String userid = params[0];
        String parkId = params[1];

        try {
            //Get Park Data
            res = api.cacheParkData(parkId, userid);
            //cache Image
            api.cacheImage(res.getParkPicture());
            //get device data
            api.getDeviceData(res, userid);

        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);

        }

        return res;
    }

    @Override
    protected void onPostExecute(final Park park) {
        //Park park = new Gson().fromJson(parkJson, Park.class);
        final String userid = DB.getString(UtilsHelper.CURRENT_USER_ID);
        ImageView parkRating = (ImageView) parkActivity.findViewById(R.id.imgParkRating);
        ImageView parkLogo = (ImageView) parkActivity.findViewById(R.id.parkImg);
        final TextView lblNumOfRating = (TextView) parkActivity.findViewById(R.id.lbl_park_num_of_rating);
        final ImageView imgFollow = (ImageView) parkActivity.findViewById(R.id.img_park_layout_follow);

        TextView lblParkName = (TextView) parkActivity.findViewById(R.id.lblParkName);
        final TextView lblNumOfFolllowers = (TextView) parkActivity.findViewById(R.id.lblNumOfFollow);
        TextView lblCity = (TextView) parkActivity.findViewById(R.id.lblParkCity);
        TextView lblNumOfActivities = (TextView) parkActivity.findViewById(R.id.lblNumOfActivities);
        final Button btnDevices = (Button) parkActivity.findViewById(R.id.btnDevices);
        final Button btnInfo = (Button) parkActivity.findViewById(R.id.btnActivitiesInfo);
        final Button btnActivities = (Button) parkActivity.findViewById(R.id.btnActivities);

        View.OnClickListener ocl = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Integer currentParkTab = DB.getInt(UtilsHelper.CURRENT_PARK_TAB) != null ? DB.getInt(UtilsHelper.CURRENT_PARK_TAB) : R.id.btnActivitiesInfo;
                switch (v.getId()) {
                    case R.id.btnDevices:
                        if (currentParkTab.intValue() != R.id.btnDevices) {
                            btnDevices.setTextColor(context.getResources().getColor(R.color.dark_orange));
                            btnActivities.setTextColor(context.getResources().getColor(R.color.x_dark_orange));
                            btnInfo.setTextColor(context.getResources().getColor(R.color.x_dark_orange));
                            fragmentManager.beginTransaction().replace(R.id.main_content, UtilsHelper.getFragment(DeviceFragment.class)).commit();
                            currentParkTab = R.id.btnDevices;
                        }
                        break;
                    case R.id.btnActivitiesInfo:
                        if (currentParkTab.intValue() != R.id.btnActivitiesInfo) {
                            btnDevices.setTextColor(context.getResources().getColor(R.color.x_dark_orange));
                            btnActivities.setTextColor(context.getResources().getColor(R.color.x_dark_orange));
                            btnInfo.setTextColor(context.getResources().getColor(R.color.dark_orange));
                            fragmentManager.beginTransaction().replace(R.id.main_content, UtilsHelper.getFragment(InfoFragment.class)).commit();
                            currentParkTab = R.id.btnActivitiesInfo;
                        }
                        break;
                }
                DB.addData(UtilsHelper.CURRENT_PARK_TAB, currentParkTab);

            }
        };

        try {
            btnDevices.setOnClickListener(ocl);
            btnInfo.setOnClickListener(ocl);
            if (!park.getIsFollower()) {
                imgFollow.setVisibility(View.VISIBLE);
                imgFollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new SetUserFollowParkHttpTask(imgFollow, lblNumOfFolllowers, parkActivity).execute(String.valueOf(park.getParkid()), userid);
                    }
                });
            }
            // Set data
            lblNumOfActivities.setText(park.getNumOfActivities().toString());
            lblCity.setText(park.getCity());
            lblParkName.setText(park.getParkName());
            lblNumOfRating.setText(park.getNumOfRating().toString());
            lblNumOfFolllowers.setText(park.getNumOfFollowers().toString());
            parkRating.setImageResource(UtilsHelper.getRatingBigImage(park.getParkRating()));
            parkLogo.setImageBitmap(images.get(park.getParkPicture()));


            // Commit fragment
            if (fragmentManager == null) {
                fragmentManager = ((FragmentActivity) parkActivity).getSupportFragmentManager();
            }
            fragmentManager.beginTransaction().replace(R.id.main_content, UtilsHelper.getFragment(InfoFragment.class)).commit();
            // Stop loading
            parkActivity.runOnUiThread(hideLoading);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
