package com.sportme.app.async;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sportme.app.AppData;
import com.sportme.app.LoginActivity;
import com.sportme.app.MainActivity;
import com.sportme.app.ParkActivity;
import com.sportme.app.R;
import com.sportme.app.model.User;
import com.sportme.app.utils.DBApi;
import com.sportme.app.utils.UtilsHelper;

/**
 * Created by pinhask on 1/19/17.
 */

public class GetUserHttpTask extends AsyncTask<String, Void, User> {

    private static final AppData DB = AppData.getInstance();
    private DBApi api = new DBApi();
    private LoginActivity loginActivity = null;


    public GetUserHttpTask(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;

    }

    protected User doInBackground(String... params) {
        final TextView lblError = (TextView) loginActivity.findViewById(R.id.txt_user_login_error);

        final String userName = params[0].trim();
        final String userPassword = params[1].trim();

        Runnable showNoRecordMessage = new Runnable() {
            @Override
            public void run() {
                lblError.setVisibility(View.VISIBLE);
                lblError.setText("No such user record in our db, have you signed in?");
            }
        };
        Runnable showBadInput = new Runnable() {
            @Override
            public void run() {
                lblError.setVisibility(View.VISIBLE);
                lblError.setText("Plead provide valid username and password");

            }
        };

        if (isLoginFormValid(userName, userPassword)) {
            // Fetch data
            User user = api.logUser(userName, userPassword);
            // If error display
            if (user == null) {
                loginActivity.runOnUiThread(showNoRecordMessage);
            }
            return user;

        } else {
            loginActivity.runOnUiThread(showBadInput);
        }

        return null;

    }

    private boolean isLoginFormValid(String text, String text1) {
        text = text.trim();
        text1 = text1.trim();

        if (text == null || text1 == null) {
            return false;
        }

        if ("".equalsIgnoreCase(text) || "".equalsIgnoreCase(text1)) {
            return false;
        }
        if (text.length() < 8 || !text.contains("@") || text1.length() < 5) {
            return false;
        }
        return true;
    }

    protected void onPostExecute(User res) {

        if(res != null) {
            DB.addData(UtilsHelper.CURRENT_USER, res);
            DB.addData(UtilsHelper.CURRENT_USER_ID, res.getUserId());
            //Todo: move to search activity
            Intent intent = new Intent(loginActivity, MainActivity.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            loginActivity.startActivity(intent);

        }

    }
}
