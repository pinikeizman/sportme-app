package com.sportme.app.async;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.sportme.app.AppData;
import com.sportme.app.utils.DBApi;

import java.util.Map;

/**
 * Created by pinhask on 1/19/17.
 */

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    private ImageView image = null;
    private DBApi api = new DBApi();
    public DownloadImageTask(ImageView bmImage) {
        this.image = bmImage;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = api.cacheImage(urldisplay);
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {

        image.setImageBitmap(result);
    }
}
