package com.sportme.app.async;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;

import com.sportme.app.adapters.SearchListAdapter;
import com.sportme.app.model.SolrListItem;
import com.sportme.app.utils.DBApi;
import com.sportme.app.utils.UtilsHelper;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pinhask on 1/27/17.
 */

public class SolrQueryHttpRequest extends AsyncTask<String, Void, SolrListItem[]> {

    ListView list;
    EditText searchString;
    Context context;

    public SolrQueryHttpRequest(ListView list, EditText searchString, Context context) {
        this.list = list;
        this.searchString = searchString;
        this.context = context;
    }

    @Override
    protected SolrListItem[] doInBackground(String... params) {

        try {
//            TODO: ADD IMPLEMENTATION WITH POST PARAMS...
//            Map<String, String> data = new HashMap<String,String>();
//            params.put("email", "first.last@example.com");
//            ResponseEntity<String> response = restTemplate.postForEntity( url, params, String.class );
            final String url = "http://"+UtilsHelper.SERVER_IP+":8080/sportme/webapi/search/entity/city:*" + params[0].trim() + "*"; //10.0.2.2 # 192.168.1.37
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            ResponseEntity<SolrListItem[]> responseEntity = restTemplate.getForEntity(url, SolrListItem[].class);
            SolrListItem[] res = responseEntity.getBody();

            return res;

        } catch (Exception e) {

            Log.e("MainActivity", e.getMessage(), e);

        }

        return new SolrListItem[0];

    }

    @Override
    protected void onPostExecute(SolrListItem[] listables) {
        try {
            if (listables == null || listables.length == 0) {
                listables = new SolrListItem[1];
                List val = new ArrayList<String>();
                val.add("N/A");
                listables[0] = new SolrListItem();
                listables[0].setName(val);
                listables[0].setDescription(val);
            }
        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }
        SearchListAdapter adp = new SearchListAdapter(context, listables);
        list.setAdapter(adp);

    }

}
