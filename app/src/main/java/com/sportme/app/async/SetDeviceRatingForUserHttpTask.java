package com.sportme.app.async;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sportme.app.AppData;
import com.sportme.app.ParkActivity;
import com.sportme.app.R;
import com.sportme.app.model.Device;
import com.sportme.app.model.Park;
import com.sportme.app.model.ParkRating;
import com.sportme.app.model.User;
import com.sportme.app.model.UserParkRating;
import com.sportme.app.utils.DBApi;
import com.sportme.app.utils.UtilsHelper;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

/**
 * Created by pinhask on 1/27/17.
 */

public class SetDeviceRatingForUserHttpTask extends AsyncTask<String, Void, Void> {

    private TextView numOfRating;
    private Context context;
    private Activity deviceActivity;
    private ImageView ratingImg;
    private AppData DB = AppData.getInstance();
    private Map<String, Park> parks = UtilsHelper.parks;
    private Map<String, Bitmap> images = UtilsHelper.images;
    private Gson gson = new Gson();
    private AlertDialog dialog;
    private DBApi api = new DBApi();

    public SetDeviceRatingForUserHttpTask(AlertDialog dialog, ImageView ratingImg, Context context, Activity parkActivity) {
        this.context = context;
        this.deviceActivity = parkActivity;
        this.dialog = dialog;
        this.ratingImg = ratingImg;
    }

    public SetDeviceRatingForUserHttpTask(AlertDialog dialog, ImageView ratingImg, TextView numOfRating, Context baseContext, Activity parkActivity) {
        this.dialog = dialog;
        this.context = baseContext;
        this.deviceActivity = parkActivity;
        this.ratingImg = ratingImg;
        this.numOfRating = numOfRating;
    }

    public Activity getDeviceActivity() {
        return (Activity) context;
    }

    @Override
    protected Void doInBackground(String... params) {

        String rating = params[0];
        final Device device = gson.fromJson(params[1], Device.class);
        String userid = params[2];

        try {

            final ResponseEntity<List> response = api.setUserRatingForDevice(device.getDeviceNumber(), rating, userid);

            if (response.getStatusCode() == HttpStatus.OK) {
                Runnable chageRating = new Runnable() {
                    @Override
                    public void run() {
                        Device deviceToChange = UtilsHelper.devicesEntity.get(device.getDeviceNumber());
                        ImageView imgRating = ratingImg;
                        imgRating.setImageResource(UtilsHelper.getRatingBigImage(String.valueOf(deviceToChange.getRating())));
                        numOfRating.setText(deviceToChange.getNumOfRating());

                    }
                };
                deviceActivity.runOnUiThread(chageRating);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dialog.dismiss();
            DB.addData(UtilsHelper.RATING_SPINNER_TOUCH, false);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void v) {

    }

}
