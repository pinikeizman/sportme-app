package com.sportme.app.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.gson.Gson;
import com.sportme.app.AppData;
import com.sportme.app.model.Device;
import com.sportme.app.model.Park;
import com.sportme.app.model.ParkRating;
import com.sportme.app.model.User;
import com.sportme.app.model.UserParkRating;
import com.sportme.app.model.RatingPK;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by pinhask on 7/28/17.
 */

public class DBApi {

    private AppData DB = AppData.getInstance();
    private Gson gson = new Gson();
    private Map<String, Park> parks = UtilsHelper.parks;
    private Map<String, Bitmap> images = UtilsHelper.images;
    private RestTemplate restTemplate = new RestTemplate();
    private final static String REST_SERVICE = "http://" + UtilsHelper.SERVER_IP + ":8080/sportme/webapi/rest/"; //10.0.2.2 #  10.100.102.3 # 192.168.1.37


    public DBApi() {
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
    }

    public ParkRating setUserRatingForPark(Park park) {
        ResponseEntity<ParkRating> response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        ParkRating res = null;
        User currentUser = (User)DB.getData(UtilsHelper.CURRENT_USER);
        UserParkRating userRating = new UserParkRating(park.getParkid(),
                currentUser.getUserId(),
                String .valueOf(park.getUserRating()));

        final String uri = REST_SERVICE + "parks/rating/"
                + park.getParkid() + "/"
                + currentUser.getUserId();
        try {
            response = restTemplate.postForEntity(uri, userRating, ParkRating.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                res = response.getBody();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
        return res;
    }

    public ResponseEntity<ParkRating> getParkRating(String parkid) {
        ResponseEntity<ParkRating> response = null;
        String url = REST_SERVICE + "parks/rating/" + parkid; //10.0.2.2 # 192.168.1.37
        try {
            ParkRating park = restTemplate.getForObject(url, ParkRating.class);
            response = new ResponseEntity<>(park, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;
    }

    public Park cacheParkData(String parkId, String userid) {
        Park result = null;
        try {
            if (!parks.containsKey(parkId)) {
                String url = REST_SERVICE + "parks/" + parkId + "/users/" + userid;
                ResponseEntity<Park> responseEntity = restTemplate.getForEntity(url, Park.class);
                result = responseEntity.getBody();
                parks.put(parkId, result);
            } else {
                result = parks.get(parkId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public Bitmap cacheImage(String url) {
        Bitmap res = null;
        try {

            if (!images.containsKey(url)) {
                InputStream in = new java.net.URL(url).openStream();
                res = BitmapFactory.decodeStream(in);
                images.put(url, res);
            } else {
                res = images.get(url);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public UserParkRating cacheUserRatingForPark(RatingPK pk) {

        //Get User UserParkRating on park
        UserParkRating userRating = (UserParkRating) DB.getData(UtilsHelper.CURRENT_USER_PARK_RATING);
        try {
            if (userRating == null) {
                String url = REST_SERVICE + "parks/rating/" + pk.getParkId() + "/" + pk.getUserId(); //10.0.2.2 # 192.168.1.37
                ResponseEntity<UserParkRating> responseEntity = restTemplate.getForEntity(url, UserParkRating.class);
                userRating = responseEntity.getBody();
                if (userRating == null) {
                    userRating = new UserParkRating(pk, null);
                }
                DB.addData(UtilsHelper.CURRENT_USER_PARK_RATING, userRating);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userRating;
    }

    public ArrayList<Device> getDeviceData(Park park, String userid) {
        ArrayList<Device> list = new ArrayList<>();
        List<Integer> devicesToCache = park.getDevicesId().get(0);
        boolean needToFatch = false;
        Iterator t = devicesToCache.iterator();
        while (t.hasNext() && !needToFatch) {
            String next = String.valueOf(t.next());
            Device temp = UtilsHelper.devicesEntity.get(next);
            if (temp == null) {
                needToFatch = true;
            } else {
                list.add(temp);
            }
        }

        if (needToFatch) {
            try {
                String url = REST_SERVICE + "parks/" + park.getParkid() + "/devices/" + userid; //10.0.2.2 # 192.168.1.37
                ResponseEntity<List> responseEntity = restTemplate.getForEntity(url, List.class);// (url, ArrayList.class);
                if (responseEntity.getStatusCode() == HttpStatus.OK) {
                    list = new ArrayList<>();
                    String json = gson.toJson(responseEntity.getBody());
                    Device[] res = gson.fromJson(json, Device[].class);
                    for (int i = 0; i < res.length; i++) {
                        Device device = res[i];
                        UtilsHelper.devicesEntity.put(String.valueOf(device.getDeviceNumber()), device);
                        list.add(device);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return list;
    }

    public ResponseEntity<List> setUserRatingForDevice(String deviceNumber, final String rating, String userid) {
        ResponseEntity response = null;
        Device device = UtilsHelper.devicesEntity.get(deviceNumber);

        final String uri = REST_SERVICE + "devices/" + device.getDeviceId() + "/rating/users/" + userid;
        try {
            response = restTemplate.postForEntity(uri, Integer.valueOf(rating), List.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                List res = (List) response.getBody();
                device.setRating(String.valueOf(res.get(0)));
                device.setUserRating(rating);
                device.setNumOfRating(String.valueOf(res.get(1)));
                UtilsHelper.devicesEntity.put(device.getDeviceNumber(), device);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
        return response;
    }

    private ResponseEntity<List> getDeviceRating(String deviceId) {
        ResponseEntity<List> response = null;
        String url = REST_SERVICE + "devices/" + deviceId + "/rating";
        try {
            List deviceRating = restTemplate.getForObject(url, List.class);
            response = new ResponseEntity<>(deviceRating, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;
    }

    public ResponseEntity<List> setUserFollowPark(String parkid, String userid) {
        ResponseEntity response = null;

        final String uri = REST_SERVICE + "parks/" + parkid + "/follow";
        try {
            response = restTemplate.postForEntity(uri, Integer.valueOf(userid), Integer.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                Integer res = (Integer) response.getBody();
                Park parkToChange = UtilsHelper.parks.get(parkid);
                parkToChange.setNumOfFollowers(res);
                UtilsHelper.parks.put(String.valueOf(parkToChange.getParkid()), parkToChange);
                response = new ResponseEntity(res, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            response = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {

        }
        return response;
    }

    public User logUser(String userName, String userPassword) {
        ResponseEntity response = null;
        User res = null;
        Map<String, String> data = new HashMap<>();
        data.put("userEmail", userName);
        data.put("userPassword", userPassword);

        final String uri = REST_SERVICE + "users/login";
        try {
            response = restTemplate.postForEntity(uri, data, User.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                res = (User) response.getBody();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
