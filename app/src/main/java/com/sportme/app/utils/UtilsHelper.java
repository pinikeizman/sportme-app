package com.sportme.app.utils;

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.sportme.app.AppData;
import com.sportme.app.DeviceFragment;
import com.sportme.app.R;
import com.sportme.app.model.Device;
import com.sportme.app.model.DeviceEntity;
import com.sportme.app.model.Park;
import com.sportme.app.model.User;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pinhask on 7/27/17.
 */

public class UtilsHelper {

    public static final String CURRENT_PARK = "current_park";
    public static final String PARKS = "parks_list";
    public static final String IMAGES = "cached_images";
    public static final String USERS = "users_list";
    public static final String CURRENT_USER = "current_user";
    public static final String CURRENT_USER_ID = "current_user_id";
    public static final String CURRENT_PARK_ID = "current_park_id";
    public static final String CURRENT_USER_PARK_RATING = "current_park_user_rating";
    public static final String RATING_SPINNER_TOUCH = "rating_spinner_touch";
    public final static String SERVER_IP = "10.100.102.3"; // Internal :: "10.0.2.2";
    public final static Map<String, Park> parks = AppData.getInstance().getData(UtilsHelper.PARKS) == null ? new HashMap<>() : (Map) AppData.getInstance().getData(UtilsHelper.PARKS);
    public final static Map<String, Bitmap> images = (Map) AppData.getInstance().getData(UtilsHelper.IMAGES) == null ? new HashMap<>() :
            (Map) AppData.getInstance().getData(UtilsHelper.IMAGES);
    private static final String DEVICESID = "devices";
    public static final String DEVICE_RATING_IMAGE = "device_rating_image";
    public static final String DEVICE_NUM_OF_RATING_TXT = "number_of_rating_txt";
    public static final String CURRENT_PARK_TAB = "current_park_tab";
    public static Map<String, Device> devicesEntity = (Map) AppData.getInstance().getData(UtilsHelper.DEVICESID) == null ? new HashMap<>() :
            (Map) AppData.getInstance().getData(UtilsHelper.DEVICESID);

    public static final String GOOGLE_API_KEY = "AIzaSyDYQu16taeSO3IjlW2rcCNxPIPfEAUdrY4";


    private AppData DB = AppData.getInstance();

    public static int getRatingBigImage(String parkRating) {
        parkRating = parkRating == null ? "e" : parkRating;
        switch (parkRating.charAt(0)) {
            case '1':
                return R.drawable.stargreen1;
            case '2':
                return R.drawable.stargreen2;
            case '3':
                return R.drawable.stargreen3;
            case '4':
                return R.drawable.stargreen4;
            case '5':
                return R.drawable.stargreen5;
            default:
                return R.drawable.stargreen5_g;
        }
    }

    public static int getRatingBigImage(Integer parkRating) {
        parkRating = parkRating == null ? -1 : parkRating;
        switch (parkRating) {
            case 1:
                return R.drawable.stargreen1;
            case 2:
                return R.drawable.stargreen2;
            case 3:
                return R.drawable.stargreen3;
            case 4:
                return R.drawable.stargreen4;
            case 5:
                return R.drawable.stargreen5;
            default:
                return R.drawable.stargreen5_g;
        }
    }

    public static int getRatingView(String parkRating) {
        parkRating = parkRating == null ? "e" : parkRating;
        switch (parkRating.charAt(0)) {
            case '1':
                return R.drawable.stargreen1_s;
            case '2':
                return R.drawable.stargreen2_s;
            case '3':
                return R.drawable.stargreen3_s;
            case '4':
                return R.drawable.stargreen4_s;
            case '5':
                return R.drawable.stargreen5_s;
            default:
                return R.drawable.stargreen5_s_g;
        }
    }

    public static View.OnTouchListener getRatingOnTouchListiner() {
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                AppData.getInstance().addData(UtilsHelper.RATING_SPINNER_TOUCH, true);
                return false;
            }
        };
    }

    public static Fragment getFragment(Class<?> deviceFragmentClass) {
        Fragment fragment = null;
        Class fragmentClass = deviceFragmentClass;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fragment;
    }
}
