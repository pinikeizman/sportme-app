package com.sportme.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sportme.app.AppData;
import com.sportme.app.ParkActivity;
import com.sportme.app.R;
import com.sportme.app.model.SolrListItem;
import com.sportme.app.utils.UtilsHelper;

/**
 * Created by pinhask on 1/31/17.
 */

public class SearchListAdapter extends ArrayAdapter<SolrListItem> {

    public SearchListAdapter(Context context, SolrListItem[] items) {
        super(context, R.layout.searchable_entity_row, items);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View theView = null;
        try {
            LayoutInflater theInflator = LayoutInflater.from(getContext());
            theView = theInflator.inflate(R.layout.searchable_entity_row, parent, false);
            //cache view
            ImageView icon = (ImageView) theView.findViewById(R.id.img_searchable_entity_logo);
            ImageView rating = (ImageView) theView.findViewById(R.id.img_searchable_entity_rating);
            TextView header = (TextView) theView.findViewById(R.id.lbl_searchable_entity_header);
            TextView body = (TextView) theView.findViewById(R.id.lbl_searchable_entity_description);
            TextView txtNumOfRating = (TextView) theView.findViewById(R.id.lbl_searchable_entity_num_of_rating);
            LinearLayout container = (LinearLayout) theView.findViewById(R.id.container_search_list);
            //get current item
            final SolrListItem park = getItem(position);
            //onclick listener
            View.OnClickListener moveToEntityPage = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), ParkActivity.class);
                    AppData.getInstance().addData(UtilsHelper.CURRENT_PARK_ID,park.getId());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getContext().startActivity(intent);
                }
            };
            //set values
            container.setOnClickListener(moveToEntityPage);
            header.setText(park.getName());
            body.setText(park.getCity());
            if (park.getSmallpic() != null) {
                byte[] decodedString = Base64.decode(park.getSmallpic(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                icon.setImageBitmap(decodedByte);
            }
            //inflate rating
            if (park.getParkRating() != null) {
                txtNumOfRating.setText(park.getNumOfUsersRating() != null ? park.getNumOfUsersRating() : "N/A");
                rating.setImageResource(UtilsHelper.getRatingView(park.getParkRating()));
            }
        } catch (Exception e) {
            Log.e("SearchListAdapter", e.getMessage());
        }
        //return value
        return theView;
    }



}
