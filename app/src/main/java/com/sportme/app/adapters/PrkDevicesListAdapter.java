package com.sportme.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sportme.app.AppData;
import com.sportme.app.R;
import com.sportme.app.async.DownloadImageTask;
import com.sportme.app.async.SetDeviceRatingForUserHttpTask;
import com.sportme.app.model.Device;
import com.sportme.app.utils.DBApi;
import com.sportme.app.utils.UtilsHelper;

import java.util.List;

/**
 * Created by pinhask on 1/31/17.
 */

public class PrkDevicesListAdapter extends ArrayAdapter<Device> {
    AppData DB = AppData.getInstance();
    private final DBApi api = new DBApi();
    private AlertDialog aletDialog = null;
    private Gson gson = new Gson();
    private Activity deviceFregmentActivity;

    public PrkDevicesListAdapter(Activity context, List<Device> items) {
        super((Context) context, R.layout.device_list_item_layout, items);
        deviceFregmentActivity = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View theView = null;
        try {

            LayoutInflater theInflator = LayoutInflater.from(getContext());
            theView = theInflator.inflate(R.layout.device_list_item_layout, parent, false);

            Device device = getItem(position);
            ImageView rating = (ImageView) theView.findViewById(R.id.img_device_list_item_rating);
            TextView numOfRating = (TextView) theView.findViewById(R.id.lbl_device_list_item_num_of_rating);
            TextView header = (TextView) theView.findViewById(R.id.lbl_device_list_item_header);
            TextView description = (TextView) theView.findViewById(R.id.lbl_device_list_item_description);
            LinearLayout rowContainer = (LinearLayout) theView.findViewById(R.id.c_device_list_item);

            header.setText(device.getDeviceName());
            description.setText(device.getDeviceType());
            rating.setImageResource(UtilsHelper.getRatingView(device.getRating()));
            numOfRating.setText(device.getStatus());
            rowContainer.setOnClickListener(openPopUpAndSetDevice(device));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return theView;
    }

    private View.OnClickListener openPopUpAndSetDevice(final Device device) {

        final View.OnClickListener rating = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create new template view
                View theView = LayoutInflater.from(getContext()).inflate(R.layout.park_rating_pupup_selection, null);
                //  Create dialog to be populated with the view.
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                Spinner spn = (Spinner) theView.findViewById(R.id.spnRating);
                final ImageView rating = (ImageView) theView.findViewById(R.id.img_rating_popup);

                // Create an ArrayAdapter using the string array and a default spinner layout
                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                        R.array.planets_array, android.R.layout.simple_spinner_item);
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                // Apply the adapter to the spinner
                spn.setAdapter(adapter);
                // Get current Device
                // Set initials Values
                spn.setOnTouchListener(UtilsHelper.getRatingOnTouchListiner());
                spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        if (DB.getData(UtilsHelper.RATING_SPINNER_TOUCH) == null
                                || !(boolean) DB.getData(UtilsHelper.RATING_SPINNER_TOUCH)) {
                            return;
                        }

                        TextView selection = (TextView) parent.getChildAt(0);
                        CharSequence choise = selection.getText();
                        new SetDeviceRatingForUserHttpTask(aletDialog,(ImageView)DB.getData(UtilsHelper.DEVICE_RATING_IMAGE),
                                (TextView) DB.getData(UtilsHelper.DEVICE_NUM_OF_RATING_TXT), getContext(), deviceFregmentActivity)
                                .execute(String.valueOf(choise),gson.toJson(device),DB.getString(UtilsHelper.CURRENT_USER_ID));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                // If user didn't rate yet.
                TextView header = (TextView) theView.findViewById(R.id.lbl_rating_popup_header);
                if (device.getUserRating() != null && !device.getUserRating().contains("null")) {
                    rating.setImageResource(UtilsHelper.getRatingBigImage(device.getUserRating()));
                    String str = device.getUserRating() != null ? device.getUserRating() : "0";
                    spn.setSelection(Integer.valueOf(str) - 1);
                } else {
                    header.setVisibility(View.INVISIBLE);
                    spn.setSelection(1);

                }

                builder.setView(theView);
                aletDialog = builder.create();
                aletDialog.show();
            }
        };


        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View theView = LayoutInflater.from(getContext()).inflate(R.layout.park_device_details_layout, null);
                ImageView deviceImage = (ImageView) theView.findViewById(R.id.img_park_device_logo);
                TextView deviceName = (TextView) theView.findViewById(R.id.lbl_park_device_name);
                TextView deviceType = (TextView) theView.findViewById(R.id.lbl_park_device_type);
                ImageView deviceRating = (ImageView) theView.findViewById(R.id.img_park_device_rating);
                TextView deviceManufucturer = (TextView) theView.findViewById(R.id.lbl_park_device_manufacturer);
                Spinner spn = (Spinner) theView.findViewById(R.id.spn_park_device_rating);
                LinearLayout container = (LinearLayout) theView.findViewById(R.id.container_park_device_header);
                TextView deviceNumOfRating = (TextView) theView.findViewById(R.id.lbl_park_device_num_of_rating);
                TextView deviceNumber = (TextView) theView.findViewById(R.id.lbl_park_device_device_number);
                TextView deviceRecommendedProgram = (TextView) theView.findViewById(R.id.lbl_park_device_recommended_program);
                TextView deviceId = (TextView) theView.findViewById(R.id.lbl_park_device_device_id);
                final ImageView deviceRatingImg = deviceRating;

                DB.addData(UtilsHelper.DEVICE_RATING_IMAGE,deviceRating);
                DB.addData(UtilsHelper.DEVICE_NUM_OF_RATING_TXT,deviceNumOfRating);

                container.setOnClickListener(rating);

                new DownloadImageTask(deviceImage).execute(device.getDevicePicture());
                deviceId.setText(device.getDeviceId());
                deviceName.setText(device.getDeviceName());
                deviceType.setText(device.getDeviceType());
                deviceRating.setImageResource(UtilsHelper.getRatingBigImage(device.getRating()));
                deviceManufucturer.setText(device.getDeviceManufacturer());
                deviceNumber.setText(device.getDeviceNumber());
                deviceRecommendedProgram.setText(device.getRecommendedProgram());

                String rating = device.getNumOfRating().contains("null") ? "0" : device.getNumOfRating() ;
                deviceNumOfRating.setText(rating);

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setView(theView);
                builder.create().show();
            }
        };

    }
}
