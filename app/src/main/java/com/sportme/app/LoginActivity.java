package com.sportme.app;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;

import android.widget.TextView;

import com.sportme.app.async.GetUserHttpTask;

import com.sportme.app.utils.DBApi;
import com.sportme.app.utils.UtilsHelper;

import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    LoginActivity loginActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UtilsHelper.parks.clear();
        UtilsHelper.devicesEntity.clear();
        setContentView(R.layout.user_login_layout);
        Button btnSubmit = (Button) findViewById(R.id.btn_user_login_submit);
        final TextView txtUserName = (TextView) findViewById(R.id.txt_user_login_user_name);
        final TextView txtUserPassword = (TextView) findViewById(R.id.txt_user_login_password);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetUserHttpTask(loginActivity).execute(txtUserName.getText().toString(), txtUserPassword.getText().toString());
            }
        });
    }


}
