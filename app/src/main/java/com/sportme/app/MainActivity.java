package com.sportme.app;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ListView;

import com.sportme.app.async.SolrQueryHttpRequest;
import com.sportme.app.model.Park;
import com.sportme.app.model.User;
import com.sportme.app.utils.UtilsHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    ListView list;
    EditText searchString;
    AppData DB = AppData.getInstance();
    Map parks = UtilsHelper.parks, images = UtilsHelper.images;

    String userName = "pini1089@gmail.com";
    String userPassward = "12345";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchString = (EditText) findViewById(R.id.txtSearch);
        list = (ListView) findViewById(R.id.entityList);
        searchString.addTextChangedListener(new TextWatcher() {
            String query = "";

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                query = cs.toString();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                if (query.trim().length() >= 3) {
                    new SolrQueryHttpRequest(list, searchString, getBaseContext()).execute(query);
                }
            }

        });

    }
}
